package com.leonlabs.master.repository.jpa.location;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.leonlabs.master.entity.location.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

	List<State> findByCountryId(Integer countryId);

}
