package com.leonlabs.master.repository.jpa.location;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.leonlabs.master.entity.location.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {


}
