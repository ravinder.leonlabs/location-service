package com.leonlabs.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@PropertySource({
    "classpath:application-master-data.properties",
    "classpath:messages.properties"
})
@ComponentScan(basePackages = "com.leonlabs")
public class MasterDataMicroservice extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(MasterDataMicroservice.class, args);
	}
}

