package com.leonlabs.master.view.location;

import com.leonlabs.core.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class LocationView extends BaseView{

	private static final long serialVersionUID = 1L;

	private String pincode;

	private Integer cityId;
	
	private Integer stateId;
	
	private Integer countryId;
	
	private String lattitude;
	
	private String longitude;
	
	private String city;
	
	private String state;
	
	private String country;
	
}
