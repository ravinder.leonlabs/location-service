package com.leonlabs.master.view.location;

import com.leonlabs.core.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class StateView extends BaseView implements GeoLocation{

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	private Integer countryId;

}
