package com.leonlabs.master.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leonlabs.core.exception.ApplicationException;
import com.leonlabs.core.rest.AbstractRestHandler;
import com.leonlabs.core.view.ResponseView;
import com.leonlabs.master.service.LocationDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/master")
@Api(tags = { "location-data-service" })
public class LocationDataController extends AbstractRestHandler {

	@Autowired
	private LocationDataService locationDataService;
	
	@RequestMapping(value = "/countries", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "All countries with matching search criteria", notes = "Maximum of 10 cities will be populated at a time", response = List.class)
	public ResponseView getAllCountries() throws Exception {
		try {
			return new ResponseView(locationDataService.getCountries());
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = "/cities", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	@ApiOperation(value = "All cities with requested state Id", notes = "All cities will be populated at a time", response = List.class)
	public ResponseView getCitiesByStateId(
			@ApiParam(value = "State Id.", required = true) @RequestParam("id") int id) throws Exception {
		try {
			return new ResponseView(locationDataService.getCitiesByStateId(id));
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	@RequestMapping(value = "/states", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	@ApiOperation(value = "All states with requested country Id", notes = "All states will be populated at a time", response = List.class)
	public ResponseView getAllStatesByCountryId(
			@ApiParam(value = "country Id.", required = true) @RequestParam("id") int id) throws Exception {
		try {
			return new ResponseView(locationDataService.getAllStatesByCountryId(id));
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
}
