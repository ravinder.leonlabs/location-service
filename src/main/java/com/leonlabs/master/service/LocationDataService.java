package com.leonlabs.master.service;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leonlabs.core.exception.ApplicationException;
import com.leonlabs.core.exception.NoResultFoundException;
import com.leonlabs.master.entity.location.City;
import com.leonlabs.master.entity.location.Country;
import com.leonlabs.master.entity.location.State;
import com.leonlabs.master.repository.jpa.location.CityRepository;
import com.leonlabs.master.repository.jpa.location.CountryRepository;
import com.leonlabs.master.repository.jpa.location.StateRepository;
import com.leonlabs.master.view.location.CityView;
import com.leonlabs.master.view.location.CountryView;
import com.leonlabs.master.view.location.StateView;

@Service
public class LocationDataService {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private MessageSource messages;
	
	@Transactional(readOnly=true)
	
	public List<CityView> getCitiesByStateId(Integer stateId) throws ApplicationException, NoResultFoundException {
		List<CityView> listCity = new ArrayList<>();
		try {
			List<City> cities = cityRepository.findByStateId(stateId);
			
			listCity = cities.stream().map(city ->{
				CityView cityVO = new CityView();
				BeanUtils.copyProperties(city, cityVO);
				return cityVO;
			}).collect(Collectors.toList());
			return listCity;
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e);
		}
	}
	
	@Transactional(readOnly=true)
	
	public List<CountryView> getCountries() throws ApplicationException, NoResultFoundException {
		List<CountryView> listCountry = new ArrayList<>();
		try {
			List<Country> countries = countryRepository.findAll();
			listCountry = countries.stream().map(country ->{
				CountryView countryVO = new CountryView();
				BeanUtils.copyProperties(country, countryVO);
				return countryVO;
			}).collect(Collectors.toList());
			return listCountry;
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e);
		}
	}

	public List<StateView> getAllStatesByCountryId(Integer countryId)
			throws ApplicationException, NoResultFoundException {
		List<StateView> listStates = new ArrayList<>();
		try {
			List<State> states = stateRepository.findByCountryId(countryId);
			listStates = states.stream().map(state ->{
				StateView stateVO = new StateView();
				BeanUtils.copyProperties(state, stateVO);
				return stateVO;
			}).collect(Collectors.toList());
			return listStates;
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e);
		}
	}

	


}
