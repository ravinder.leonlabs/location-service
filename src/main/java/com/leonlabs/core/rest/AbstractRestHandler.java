package com.leonlabs.core.rest;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.leonlabs.core.exception.ApplicationException;
import com.leonlabs.core.exception.AuthenticationException;
import com.leonlabs.core.exception.DataFormatException;
import com.leonlabs.core.exception.NoResultFoundException;
import com.leonlabs.core.exception.RequestValidationException;
import com.leonlabs.core.exception.ResourceNotFoundException;
import com.leonlabs.core.view.ErrorView;
import com.leonlabs.core.view.HTTPStatus;
import com.leonlabs.core.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

/**
 * This class is meant to be extended by all REST resource "controllers". It
 * contains exception mapping and other common REST API functionality
 */
@ControllerAdvice
@Slf4j
public abstract class AbstractRestHandler implements ApplicationEventPublisherAware {

	protected ApplicationEventPublisher eventPublisher;
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(NoSuchMessageException.class)
	public ResponseView handleNoSuchMessageException(NoSuchMessageException ex, HttpServletRequest request){
		log.error("Error occured while executing URL : "+request.getRequestURL(),ex);
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR.getName(), ex.getMessage()));
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(SQLException.class)
	public ResponseView handleSQLException(SQLException ex, HttpServletRequest request){
		log.error("Error occured while executing URL : "+request.getRequestURL(),ex);
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR.getName(), ex.getMessage()));
	}
	
	@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="IOException occured")
	@ExceptionHandler(IOException.class)
	public ResponseView handleIOException(IOException ex, HttpServletRequest request){
		log.error("Error occured while executing URL : "+request.getRequestURL(),ex);
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR.getName(), ex.getMessage()));
	}
	
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseView handleException(Exception ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(DataFormatException.class)
	public @ResponseBody ResponseView handleDataFormatException(DataFormatException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.BAD_REQUEST.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler(AuthenticationException.class)
	public @ResponseBody ResponseView handleAuthenticationException(AuthenticationException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.FORBIDDEN, null,new ErrorView(HTTPStatus.FORBIDDEN.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(RuntimeException.class)
	public @ResponseBody ResponseView handleRuntimeException(RuntimeException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.INTERNAL_SERVER_ERROR, null,
				new ErrorView(HTTPStatus.INTERNAL_SERVER_ERROR.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResourceNotFoundException.class)
	public @ResponseBody ResponseView handleResourceNotFoundException(ResourceNotFoundException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.NOT_FOUND, null,
				new ErrorView(HTTPStatus.NOT_FOUND.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoResultFoundException.class)
	public @ResponseBody ResponseView handleNoResultFoundException(NoResultFoundException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.NOT_FOUND, null,
				new ErrorView(HTTPStatus.NOT_FOUND.getName(), ex.getMessage()));
	}

	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(RequestValidationException.class)
	public @ResponseBody ResponseView handleRequestValidationException(RequestValidationException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.PRECONDITION_FAILED, null,
				new ErrorView(HTTPStatus.NOT_FOUND.getName(), ex.getMessage()));
	}
	
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler(ApplicationException.class)
	public @ResponseBody ResponseView handleApplicationException(ApplicationException ex, HttpServletRequest request) {
		ex.printStackTrace();
		return new ResponseView(HTTPStatus.PRECONDITION_FAILED, null,
				new ErrorView(HTTPStatus.PRECONDITION_FAILED.getName(), ex.getMessage()));
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.eventPublisher = applicationEventPublisher;
	}

}