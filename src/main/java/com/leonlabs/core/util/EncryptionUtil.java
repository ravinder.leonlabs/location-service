package com.leonlabs.core.util;

import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtil {

	private static String secretKey = "s3cr3t-3ncr4pt!0n-k34";
	private static String salt = "3ncr4pt!0n-5A1T";
	private static String ENCRYPTION_TYPE = "AES";
	private static String SECRET_KEY_TYPE= "PBKDF2WithHmacSHA256";
	private static String CIPHER_TYPE = "AES/CBC/PKCS5PADDING";
	private static String ENCODING_TYPE = "UTF-8";

	public static String encrypt(String strToEncrypt) throws Exception {

		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		IvParameterSpec ivspec = new IvParameterSpec(iv);

		SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_TYPE);
		KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), ENCRYPTION_TYPE);

		Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
		return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(ENCODING_TYPE)));
	}

	public static String decrypt(String strToDecrypt) throws Exception {
		
		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		IvParameterSpec ivspec = new IvParameterSpec(iv);

		SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_TYPE);
		KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), ENCRYPTION_TYPE);

		Cipher cipher = Cipher.getInstance(CIPHER_TYPE);
		cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
		return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
	}

	public static void main(String[] args) throws Exception {

		String originalString = "Qwerty1!";
		String encryptedString = encrypt(originalString);
		String decryptedString = decrypt(encryptedString);

		System.out.println(originalString);
		System.out.println(encryptedString);
		System.out.println(decryptedString);
	}
}