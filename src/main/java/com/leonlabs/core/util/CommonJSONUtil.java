package com.leonlabs.core.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommonJSONUtil<K> {

	@SuppressWarnings("rawtypes")
	private static CommonJSONUtil util = new CommonJSONUtil();

	private CommonJSONUtil() {

	}

	@SuppressWarnings({ "rawtypes" })
	public static CommonJSONUtil getInstance() {
		return util;
	}

	/**
	 * @throws IOException 
	 * 
	 */
	public K buildObject(String indexerJSONString, Class<K> entityClazz) throws IOException {
		K indexerData;
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			indexerData = mapper.readValue(indexerJSONString, entityClazz);

			return indexerData;

		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new IOException(e);
		}
	}

	public String getJSONString(K json) {
		ObjectMapper mapper = new ObjectMapper();
		try {

			String jsonFormatstr = mapper.writeValueAsString(json);
			log.info(jsonFormatstr);
			return jsonFormatstr;

		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
