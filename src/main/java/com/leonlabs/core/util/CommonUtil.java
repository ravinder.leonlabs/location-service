package com.leonlabs.core.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

public class CommonUtil {

	public static String[] split(String s) {
		return split(s, ",");
	}

	public static String[] split(String string, String separator) {
		if (string != null)
			return string.split(separator);
		return null;
	}

	private static Random randomGen = new Random();

	public static String randomString(int length) {
		int[][] alphaChars = { { 65, 90 }, { 97, 122 } };
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int[] range = alphaChars[randomGen.nextInt(1)];
			sb.append((char) nextIntInRange(range[0], range[1]));
		}
		return sb.toString();
	}

	public static int nextIntInRange(int min, int max) {
		return min + randomGen.nextInt(max - min);
	}

	public static String generateOTP(int len) {
		System.out.println("Generating password using random() : ");
		System.out.print("Your new password is : ");

		//String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		//String Small_chars = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";
		//String symbols = "!@#$%^&*_=+-/.?<>)";

		//String values = Capital_chars + Small_chars + numbers + symbols;
		String values = numbers;

		// Using random method
		Random rndm_method = new Random();

		char[] password = new char[len];
		String otp = "";

		for (int i = 0; i < len; i++) {
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			password[i] = values.charAt(rndm_method.nextInt(values.length()));
			otp = otp+values.charAt(rndm_method.nextInt(values.length()));
		}
		return otp;
	}
	
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		Integer length = 1000000;
		for (int i = 0; i < length; i++) {
			String generateOTP = generateOTP(6);
			set.add(generateOTP);
			System.out.println(generateOTP);
		}
		System.out.println(length - set.size());
		
	}

	public static String getAppUrl(HttpServletRequest request) {
		return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}

}