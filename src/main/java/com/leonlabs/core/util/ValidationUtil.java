package com.leonlabs.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;


public class ValidationUtil {
	
	public static boolean isNull(String value){
		return StringUtils.isEmpty(value);
	}
	public static boolean isEmpty(String value){
		return StringUtils.isEmpty(value);
	}
	
	public static boolean isNullOrEmpty(String ... value){
		for (String s : value) {
			if(StringUtils.isEmpty(s)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean isValidPassword(String value,String pattern){
		if(isNullOrEmpty(pattern)){
			pattern=ValidationPattern.Pattern.PASSWORD.getValue();
		}
		return isValid(value,pattern);
	}
	public static boolean isValidEmail(String value){
		return isValid(value,ValidationPattern.Pattern.EMAIL.getValue());
	}
	public static boolean isValid(String value,String pattern){
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(value);
		return m.matches();
	}
	
	private static class ValidationPattern {
		 enum Pattern {
			PASSWORD("", 0), 
			USERNAME("", 1),
			EMAIL("^[A-Za-z]+([_A-Za-z0-9.][A-Za-z0-9]+)*@[A-Za-z0-9]+([A-Za-z0-9-][A-Za-z0-9]+)+\\.[a-zA-Z]{2}.?([a-zA-Z]+)$",2),
			PHONE("",3);
	
			private final String value;
			private final int index;
	
			private Pattern(String val, int index) {
				this.value = val;
				this.index = index;
			}
	
			@Override
			public String toString() {
				return this.value;
			}
	
			public String getValue() {
				return value;
			}
	
			@SuppressWarnings("unused")
			public int getIndex() {
				return index;
			}
	
			@SuppressWarnings("unused")
			public static Pattern getPattern(String key) {
				if (key != null) {
					for (Pattern p : Pattern.values()) {
						if (key.equalsIgnoreCase(p.value)) {
							return p;
						}
					}
				}
				return null;
	
			}
		}
	}
	
	
	

}
