package com.leonlabs.core.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ErrorView extends BaseView{

	private static final long serialVersionUID = 1L;

	private String errorCode;

	private String errorMessage;
	
}
