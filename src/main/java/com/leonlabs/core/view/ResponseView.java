package com.leonlabs.core.view;

import java.util.List;

import com.leonlabs.core.constant.CommonConstants;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseView {

	private HTTPStatus httpStatus; //http status code
	private String responseStatus;

	private Object data;
	private ErrorView errorMessage;
	private List<ErrorView> errorMessages;

	public ResponseView(Object data) {
		super();
		this.httpStatus = HTTPStatus.OK;
		this.responseStatus = CommonConstants.STATUS_SUCCESS;
		this.data = data;
		this.errorMessage = null;
		this.errorMessages=null;
	}

	public ResponseView(HTTPStatus httpStatus, Object data, ErrorView errorMessage) {
		super();
		this.httpStatus = httpStatus;
		this.data = data;
		this.errorMessage = errorMessage;
	}

}
